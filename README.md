# dtime-js

dtime - Erisian (Discordian) time and date
based on REAL-TIME CLOCK by Tiago Madeira

Discordian date:
https://en.wikipedia.org/wiki/Discordian_calendar

Discordian time is the decimal time without time zones, where the
beginning of the day coincides with the beginning of the Christian
winter day on Easter Island (UTC-5):
https://discordia.fandom.com/wiki/Erisian_Time