/*
* https://gitlab.com/zlax/dtime-js discrodian date and time converter
*/

function showconv() {
    document.getElementById("dconv").innerHTML = converterhtml();
    var currentdate = new Date();
    document.getElementById("ctimeoffset").value = currentdate.getTimezoneOffset();
    filltimes(currentdate);
}

function timeclick() {
    if (window.dtimescreen) window.dtimescreen++;
    else window.dtimescreen = 1;
    switch (window.dtimescreen) {
	case 1:
	    showconv();
	    break;
	case 2:
	    document.getElementById("dconv").innerHTML = "";
	    document.getElementById("dtimesec").classList.toggle('secsize');
	    break;
	case 3:
	    document.getElementById("dtimesec").classList.toggle('secsize');
	    document.getElementById("dtimesec").classList.toggle('hidden');
	    break;
	case 4:
	    document.getElementById("dtimesec").classList.toggle('hidden');
	    window.dtimescreen = 0;
	    break;
    }
}

function filltimes(convdate) {
    var dtimenum = dtime(convdate, "ddatetime");
    document.getElementById("dtimeyold").value = dtimenum[0];
    document.getElementById("dtimeseason").value = dtimenum[1];
    document.getElementById("dtimeday").value = dtimenum[2];
    document.getElementById("dtimehour").value = dtimenum[3];
    document.getElementById("dtimeminute").value = dtimenum[4];
    document.getElementById("dtimesecond").value = dtimenum[5];
    var editeddate = new Date (convdate.getTime() - (document.getElementById("ctimeoffset").value * 60000));
    document.getElementById("ctimeyear").value = editeddate.getUTCFullYear();
    document.getElementById("ctimemonth").value = editeddate.getUTCMonth() + 1;
    document.getElementById("ctimeday").value = editeddate.getUTCDate();
    document.getElementById("ctimehour").value = editeddate.getUTCHours();
    document.getElementById("ctimeminute").value = editeddate.getUTCMinutes();
    document.getElementById("ctimesecond").value = editeddate.getUTCSeconds();
}

function converttimes(fromto) {
    switch (fromto) {
	case "eris":
	    var dtimeseason = document.getElementById("dtimeseason").value - 1;
	    var dtimeday = document.getElementById("dtimeday").value - 1;
	    var ctimeyear = document.getElementById("dtimeyold").value - 1166;
	    var timestamp = Date.UTC(ctimeyear, 0, 1, 0, 0, 0) +
		(dtimeseason * 6307200000) +
		(dtimeday * 86400000) +
		(document.getElementById("dtimehour").value * 8640000) +
		(document.getElementById("dtimeminute").value * 86400) +
		(document.getElementById("dtimesecond").value * 864) + 18000000;
	    if (ly(ctimeyear)) {
		if ((dtimeseason == 0) && (dtimeday > 58) || (dtimeseason > 0)) {
		    timestamp += 86400000;
		}
	    }
	    filltimes(new Date(timestamp));
	    break;
	case "chri":
	    var ctime = new Date(Date.UTC(document.getElementById("ctimeyear").value,
		document.getElementById("ctimemonth").value - 1,
		document.getElementById("ctimeday").value,
		document.getElementById("ctimehour").value,
		document.getElementById("ctimeminute").value,
		document.getElementById("ctimesecond").value) + document.getElementById("ctimeoffset").value * 60000);
	    filltimes(ctime);
	    break;
    }    
}

function converterhtml() {
    var convinnerhtml = `
<div>Erisian:</div>
<table><tr>
    <td>yold</td>
    <td>season</td>
    <td>day</td>
    <td>hour</td>
    <td>minute</td>
    <td>second</td>
</tr><tr>
    <td><input id="dtimeyold" type="number" min=2000 max=5000 onchange="converttimes('eris')"></td>
    <td><input id="dtimeseason" type="number" min=1 max=5 onchange="converttimes('eris')"></td>
    <td><input id="dtimeday" type="number" min=1 max=73 onchange="converttimes('eris')"></td>
    <td><input id="dtimehour" type="number" min=0 max=9 onchange="converttimes('eris')"></td>
    <td><input id="dtimeminute" type="number" min=0 max=99 onchange="converttimes('eris')"></td>
    <td><input id="dtimesecond" type="number" min=0 max=99 onchange="converttimes('eris')"></td>
</tr></table>
<div>Christian:</div>
<table><tr>
    <td>year</td>
    <td>month</td>
    <td>day</td>
    <td>hour</td>
    <td>minute</td>
    <td>second</td>
</tr><tr>
    <td><input id="ctimeyear" type="number" min=834 max=3834 onchange="converttimes('chri')"></td>
    <td><input id="ctimemonth" type="number" min=1 max=12 onchange="converttimes('chri')"></td>
    <td><input id="ctimeday" type="number" min=1 max=31 onchange="converttimes('chri')"></td>
    <td><input id="ctimehour" type="number" min=0 max=23 onchange="converttimes('chri')"></td>
    <td><input id="ctimeminute" type="number" min=0 max=59 onchange="converttimes('chri')"></td>
    <td><input id="ctimesecond" type="number" min=0 max=59 onchange="converttimes('chri')"></td>
</tr></table>
<p><select id="ctimeoffset" onchange="converttimes('chri')">
    <option value="720">(UTC-12:00) Eniwetok, Kwajalein</option>
    <option value="660">(UTC-11:00) Midway Island, Samoa</option>
    <option value="600">(UTC-10:00) Hawaii</option>
    <option value="540">(UTC-9:00) Alaska</option>
    <option value="480">(UTC-8:00) Pacific Time (US &amp; Canada)</option>
    <option value="420">(UTC-7:00) Mountain Time (US &amp; Canada)</option>
    <option value="360">(UTC-6:00) Central Time (US &amp; Canada), Mexico City</option>
    <option value="300">(UTC-5:00) Eastern Time (US &amp; Canada), Bogota, Lima</option>
    <option value="240">(UTC-4:00) Atlantic Time (Canada), Caracas, La Paz</option>
    <option value="210">(UTC-3:30) Newfoundland</option>
    <option value="180">(UTC-3:00) Brazil, Buenos Aires, Georgetown</option>
    <option value="120">(UTC-2:00) Mid-Atlantic</option>
    <option value="60">(UTC-1:00 hour) Azores, Cape Verde Islands</option>
    <option value="0">(UTC) Western Europe Time, London, Lisbon, Casablanca</option>
    <option value="-60">(UTC+1:00 hour) Brussels, Copenhagen, Madrid, Paris</option>
    <option value="-120">(UTC+2:00) Kaliningrad, South Africa</option>
    <option value="-180">(UTC+3:00) Baghdad, Riyadh, Moscow, St. Petersburg</option>
    <option value="-210">(UTC+3:30) Tehran</option>
    <option value="-240">(UTC+4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>
    <option value="-270">(UTC+4:30) Kabul</option>
    <option value="-300">(UTC+5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>
    <option value="-330">(UTC+5:30) Bombay, Calcutta, Madras, New Delhi</option>
    <option value="-345">(UTC+5:45) Kathmandu</option>
    <option value="-360">(UTC+6:00) Almaty, Dhaka, Colombo</option>
    <option value="-420">(UTC+7:00) Bangkok, Hanoi, Jakarta</option>
    <option value="-480">(UTC+8:00) Beijing, Perth, Singapore, Hong Kong</option>
    <option value="-540">(UTC+9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>
    <option value="-570">(UTC+9:30) Adelaide, Darwin</option>
    <option value="-600">(UTC+10:00) Eastern Australia, Guam, Vladivostok</option>
    <option value="-660">(UTC+11:00) Magadan, Solomon Islands, New Caledonia</option>
    <option value="-720">(UTC+12:00) Auckland, Wellington, Fiji, Kamchatka</option>
</select></p>
`;
    return convinnerhtml;
}

document.addEventListener("DOMContentLoaded", function() {
    if (window.location.hash == "#c" || window.location.hash == "#conv" || window.location.hash == "#converter") {
	window.dtimescreen = 1;
	showconv();
    }
    if (window.location.hash == "#d" || window.location.hash == "#dark") {
	document.body.style.background = '#2e3235';
    }
});
