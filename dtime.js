/*
* https://gitlab.com/zlax/dtime-js
* based on:
*
* REAL-TIME CLOCK (c) 2009 por Tiago Madeira (http://tiagomadeira.com/)
* Idealizado por Santaum (http://santaum.org/)
*
* All Hail Eris!
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function az(x) {
    if (x < 10)
        return "0"+x;
    return x;
}

function ly(Y) {
    return (Y % 4 == 0 && !(Y % 100 == 0 && Y % 400 != 0));
}

function dy(D, M, Y) {
    var dm = Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    if (ly(Y)) {
        dm[1] = 29;
    }
    var d = D;
    for (var i = 0; i < M-1; i++) {
        d+= dm[i]
    }
    return d;
}

function dtime(gregorianDate,resultar="default") {
    // Easter Island Winter Time Zone offset (-5*60*60*1000)
    var date = new Date(gregorianDate.getTime()-18000000);

    var D = date.getUTCDate();
    var M = date.getUTCMonth()+1;
    var Y = date.getUTCFullYear();
    var d = dy(D, M, Y);
    var sttiby = 0;
    if (ly(Y)) {
        if (d == 60) {
            sttiby = 1;
        } else if (d > 60) {
            d--;
        }
    }
    M = Math.floor(d/73);
    D = d % 73;
    if (D == 0) {
        M--;
        D = 73;
    }
    var ddaystr = dday(D,M,d,sttiby);
    var seasonnum = M+1;
    switch (M) {
        case 0:
            M = "Chaos";
            break;
        case 1:
            M = "Discord";
            break;
        case 2:
            M = "Confusion";
            break;
        case 3:
            M = "Bureaucracy";
            break;
        case 4:
            M = "Aftermath";
            break;
        default:
            M = "fnord";
    }
    Y+= 1166;
    if (sttiby) {
        var ddatestr = "St. Tib's Day, "+Y+" YOLD";
    } else {
        var ddatestr = M+" "+az(D)+", "+Y+" YOLD";
    }

    var christianMSDay = date.valueOf() % 86400000;
    var ds = (christianMSDay/864);
    var h = Math.floor(ds/10000);
    ds%= 10000;
    var m = Math.floor(ds/100);
    ds%= 100;
    var s = Math.floor(ds);

    switch (resultar) {
        case "shortsec":
            return [ddatestr, h+":"+az(m), ":"+az(s), ddaystr];
            break;
        case "ddatetime":
            return [Y, seasonnum, D, h, m, s];
            break;
        default:
            var dtimestr = h+":"+az(m)+":"+az(s);
            return [ddatestr, dtimestr, ddaystr];
            break;
    }
}

function dday(day, season, yearday, sttiby) {
    var day_list = ["Sweetmorn", "Boomtime", "Pungenday", "Prickle-Prickle", "Setting Orange"];
    var dweekday = day_list[((yearday-1)%5)];
    var ddaystr = dweekday;
    var a_holiday = ["Mungday", "Mojoday", "Syaday", "Zaraday", "Maladay"];
    var s_holiday = ["Chaoflux", "Discoflux", "Confuflux", "Bureflux", "Afflux"];
    var m_holiday = ["Chaomas", "Discomas", "Confumas", "Buremas", "Afmas"];
    var t_holiday = ["Chaosloth", "Discosloth", "Confusloth", "Buresloth", "Afsloth"];
    var e_holiday = ["Mungeye", "Mojeye", "Syadeye", "Zareye", "Maleye"];
    if (sttiby) ddaystr = "Celebrate St. Tib's Day";
        else if (day == 5) ddaystr += ", Celebrate " + a_holiday[season];
        else if (day == 23) ddaystr += ", Celebrate " + m_holiday[season];
        else if (day == 27) ddaystr += ", Celebrate " + t_holiday[season];
        else if (day == 50) ddaystr += ", Celebrate " + s_holiday[season];
        else if (day == 73) ddaystr += ", Celebrate " + e_holiday[season];
        else if (season == 0 && day == 8) ddaystr += ", Celebrate Death of Emperor Norton";
        else if (season == 0 && day == 10) ddaystr += ", Celebrate Backwards/Binary Day";
        else if (season == 0 && day == 11) ddaystr += ", Celebrate RAW Day";
        else if (season == 0 && day == 14) ddaystr += ", Celebrate Golden Apple Presentation Day";
        else if (season == 0 && day == 17) ddaystr += ", Celebrate Joshmas";
        else if (season == 0 && day == 18) ddaystr += ", Celebrate Pat Pineapple Day";
        else if (season == 0 && day == 21) ddaystr += ", Celebrate Hug Day";
        else if (season == 0 && day == 26) ddaystr += ", Celebrate yaD sdrawkcaB, Traditional";
        else if (season == 0 && day == 37) ddaystr += ", Celebrate Aaron Burr's Birthday";
        else if (season == 0 && day == 51) ddaystr += ", Celebrate Pet Loving Day";
        else if (season == 0 && day == 69) ddaystr += ", Celebrate Chicken Head Day";
        else if (season == 0 && day == 72) ddaystr += ", Celebrate Daytime";
        else if (season == 1 && day == 4) ddaystr += ", Celebrate Grover Cleveland's Birthday";
        else if (season == 1 && day == 11) ddaystr += ", Celebrate Discordians for Jesus/Love Your Neighbor Day";
        else if (season == 1 && day == 18) ddaystr += ", Celebrate Fool's Day";
        else if (season == 1 && day == 19) ddaystr += ", Celebrate St. John the Blasphemist's Day";
        else if (season == 1 && day == 34) ddaystr += ", Celebrate Omarmas";
        else if (season == 1 && day == 43) ddaystr += ", Celebrate Universal Ordination Day";
        else if (season == 1 && day == 68) ddaystr += ", Celebrate Mal-2mas";
        else if (season == 1 && day == 70) ddaystr += ", Celebrate Day of the Elppin/Defenestration of Prague Day";
        else if (season == 1 && day == 72) ddaystr += ", Celebrate Towel Day";
        else if (season == 2 && day == 26) ddaystr += ", Celebrate Imaginary Friend Day";
        else if (season == 2 && day == 28) ddaystr += ", Celebrate St. George's Day";
        else if (season == 2 && day == 30) ddaystr += ", Celebrate Zoog Day";
        else if (season == 2 && day == 37) ddaystr += ", Celebrate Mid-Year's Day";
        else if (season == 2 && day == 40) ddaystr += ", Celebrate X-Day";
        else if (season == 2 && day == 55) ddaystr += ", Celebrate Mal-2 Day";
        else if (season == 2 && day == 57) ddaystr += ", Celebrate John Dillinger Day";
        else if (season == 3 && day == 3) ddaystr += ", Celebrate Multiversal Underwear Day";
        else if (season == 3 && day == 10) ddaystr += ", Celebrate St. Cecil Day";
        else if (season == 3 && day == 18) ddaystr += ", Celebrate Spanking Fest";
        else if (season == 3 && day == 33) ddaystr += ", Celebrate Pussyfoot Day";
        else if (season == 3 && day == 37) ddaystr += ", Celebrate Mass of Planet Eris/Mass of Eristotle";
        else if (season == 3 && day == 39) ddaystr += ", Celebrate St. Mammes's Day";
        else if (season == 3 && day == 41) ddaystr += ", Celebrate Emperor Norton Proclamation Day";
        else if (season == 3 && day == 55) ddaystr += ", Celebrate Feast of St. John Blasphemist";
        else if (season == 3 && day == 57) ddaystr += ", Celebrate Shamlicht Kids Club Day";
        else if (season == 3 && day == 59) ddaystr += ", Celebrate Gonculator Day";
        else if (season == 3 && day == 60) ddaystr += ", Celebrate Mad Hatter Day";
        else if (season == 3 && day == 66) ddaystr += ", Celebrate Habeas Corpus Remembrance Day";
        else if (season == 4 && day == 17) ddaystr += ", Celebrate Pope Night";
        else if (season == 4 && day == 28) ddaystr += ", Celebrate Ek-sen-triks CluborGuild Day";
        else if (season == 4 && day == 37) ddaystr += ", Celebrate 537 Day";
        else if (season == 4 && day == 40) ddaystr += ", Celebrate Omar Day";
        else if (season == 4 && day == 43) ddaystr += ", Celebrate Day D";
        else if (season == 4 && day == 46) ddaystr += ", Celebrate Hug Day II";
        else if (season == 4 && day == 56) ddaystr += ", Celebrate Agnostica";
        else if (season == 4 && day == 65) ddaystr += ", Celebrate Circlemas";
    return ddaystr;
}

function go() {
    var iddate = document.getElementById("ddate");
    var idtimeshort = document.getElementById("dtimeshort");
    var idtimesec = document.getElementById("dtimesec");
    var idday = document.getElementById("dday");
    // Current date-time with any time zone offset
    var ddatetime = new Date();
    ddatetime = dtime(ddatetime, "shortsec");
    iddate.innerHTML = ddatetime[0];
    idtimeshort.innerHTML = ddatetime[1];
    idtimesec.innerHTML = ddatetime[2];
    idday.innerHTML = ddatetime[3];
}

window.onload = function() {
    go();
    // 1 discordian decimal second = 864 christian milliseconds
    setInterval(go, 864);
}
